const db = require('../models');

module.exports = {
    create: (req, res, next) => {
        const data = {
            idc: req.body.idc,
            lastname: req.body.lastname,
            firstname: req.body.firstname,
            email: req.body.email,
            password: req.body.password
        };
        return db.User.create(data)
        .then((user) => res.json(user))
        .catch((err) => next(err));
    },
    get_by_id: (req, res, next) => {
        return  db.User.findByPk(req.params.user_id)
        .then((user) => {
            if (!user)
                throw {status: 404, message: "User not found"};
            return res.json(user);
        })
        .catch((err) => next(err));
    },
    load_by_id: (req, res, next) => {
        return db.User.findByPk(req.params.user_id)
        .then((user) => {
            if (!user)
                throw {status: 404, message: "User not found"};
            req.user = user;
            return next();
        })
        .catch((err) => next(err));
    },
    login: (req, res, next) => {
        return  db.User.findByPk(req.params.user_id)
        .then(async (user) => {
            if(!user)
                res.redirect('/login');
            else if (!await user.check_password(req.params.password))
                res.redirect('/login');
            else
                res.redirect('/home');
        })
        .catch((err) => next(err));
    }
};
const db = require('../models');

module.exports = {
    create: (req, res, next) => {
        const data = {
            event_name: req.body.event_name,
            due_date: req.body.due_date
        };
        return db.Event.create(data)
        .then((event) => res.json(event))
        .catch((err) => next(err));
    },
    get_by_user_id: (req, res, next) => {
        return req.user.getEvents()
        .then((events) => res.json(events))
        .catch((err) => next(err));
    }
};
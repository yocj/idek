const user_ctrl = require('../controllers/user');

module.exports = [
    {
        url: '/register',
        method: 'post',
        func: user_ctrl.create
    },
    {
        url: '/login',
        method: 'post',
        func: user_ctrl.login
    }
];
const event_ctrl = require('../controllers/event');
const user_ctrl = require('../controllers/user');

module.exports = [
    {
        url: '/newevent',
        method: 'post',
        func: event_ctrl.create
    },
    {
        url: '/user/:user_id/events',
        method: 'get',
        func: [user_ctrl.load_by_id, event_ctrl.get_by_user_id]
    }
];
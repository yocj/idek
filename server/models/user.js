const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const user = require('../controllers/user');

module.exports = (sequelize) => {

    class User extends Sequelize.Model {
        async check_passwd(password) {
            return await bcrypt.compare(password, this.passwd);
        };
        static associate(db) {
            User.belongsToMany(db.Event, {through: 'UserEvent'});
        };
    }
    User.init({
        idc: Sequelize.STRING,
        firstname: Sequelize.STRING,
        lastname: Sequelize.STRING,
        email: {
            type: Sequelize.STRING,
            validate: {
                isEmail: true
            }
        },
        passwd: Sequelize.STRING,
    }, {
        sequelize,
        hooks: {
            beforeValidate: async (user) => {
                const salt = await bcrypt.genSalt(10);
                user.passwd = await bcrypt.hash(user.password, salt);
            },
        },
        modelName: 'User'
    });
    return User;
};
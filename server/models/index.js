const fs = require('fs');
const Sequelize = require('sequelize');

const sequelize = new Sequelize('idek', 'admin', 'admin', {
    host: 'localhost',
    port: '3306',
    dialect: 'mariadb',
    dialectOptions: {decimalNumbers: true}
});

const db = {};

fs.readdirSync(__dirname)
.filter((filename) => filename !== 'index.js')
.forEach((filename) => {
	const model = require('./' + filename)(sequelize);
	db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
	db[modelName].associate(db);
});

sequelize.sync();

module.exports = db;

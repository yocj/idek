const Sequelize = require('sequelize');

module.exports = (sequelize) => {

    class Event extends Sequelize.Model {
        static associate(db) {
            Event.belongsToMany(db.User, {through: 'UserEvent'});
        };
    }

    Event.init({
        event_name: Sequelize.STRING,
        due_date: Sequelize.DATE
    }, {
        sequelize,
        modelName: 'Event'
    });
    return Event;
};